-- add initial Vendor test data
--public class Vendor {
--    @Id
--    @GeneratedValue(strategy = GenerationType.IDENTITY)
--    private long Id;
--private String name;
--private String address1;
--private String city;
--private String province;
--private String postalcode;
--private String phone;
--private String type;
--private String email;
--}
INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
VALUES ('ABC Supply Co.','Smartypants','city','province','N5Y 3E6','(555)555-5551','sometype','bs@abc.com');
INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
VALUES ('Big Bill Depot','Smartypants','city','province','N5Y 3E6','(555)555-5551','sometype','bs@abc.com');
INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
VALUES ('Shady Sams','Smartypants','city','province','N5Y 3E6','(555)555-5551','sometype','bs@abc.com');
INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
VALUES ('Shady Sams2','Smartypants','city','province','N5Y 3E6','(555)555-5551','sometype','bs@abc.com');
INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
VALUES ('Shady Sams3','Smartypants','city','province','N5Y 3E6','(555)555-5551','sometype','bs@abc.com');
INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
VALUES ('Jinzhi Yang','922 Oak St','London','On', 'N1N-1N1','(555)555-5599','Un Trusted','ss@underthetable.com');


-- private String Id;
-- private int vendorid;   //FK
-- private String name;
-- private BigDecimal costprice;
-- private BigDecimal msrp;
-- private int rop;
-- private int eoq;
-- private int qoh;
-- private int qoo;
-- private String qrcode;  //for case2
-- private String qrcodetxt;   //for case2

INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X15',1,'Egg',11,120,1,11,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '16X16',6,'Coke #2',129.97,140,1,10,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '16X17',6,'Sprite #3',329.98,150,1,5,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '16X18',6,'Beer #4',44,120,1,44,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X19',2,'Coke #5',55,120,1,55,20,0 );
-- INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
-- VALUES ('Penny','Pincher','city','province','(555)555-5551','sometype','pp@abc.com');
-- INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
-- VALUES ('Smoke','Andmirrors','city','province','(555)555-5552','sometype','sa@abc.com');
-- INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
-- VALUES ('Jinzhi Yang','922 Oak St','London','On', 'N1N-1N1','(555)555-5599','Un Trusted','ss@underthetable.com');
-- INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
-- VALUES ('Sam','Slick','city','province','(555)555-5552','sometype','ng@abc.com');
-- INSERT INTO Vendor (Name,Address1,City,Province,PostalCode,Phone,Type,Email)
-- VALUES ('Sloppy','Joe','city','province','(555)555-5553','sometype','sj@abc.com');


INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X191',1,'Naked Grape #1',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X192',2,'Naked Grape #2',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X193',3,'Naked Grape #5',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X194',4,'Naked Grape #6',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X195',5,'Johnnie Walker #5',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X196',6,'Johnnie Walker #6',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X197',6,'Johnnie Walker #4',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X198',5,'Johnnie Walker #3',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X199',4,'Johnnie Walker #2',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X200',3,'Johnnie Walker #1',55,120,1,55,20,0 );
INSERT  INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo)
VALUES ( '14X201',2,'Johnnie Walker #7',55,120,1,55,20,0 );
