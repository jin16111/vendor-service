package com.info5059.casestudy.purchaseorder;

import com.info5059.casestudy.product.Product;
import com.info5059.casestudy.product.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Component
public class PurchaseOrderDAO {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    ProductRepository pr;
    @Transactional
    public Long create(PurchaseOrder newOrder) {
        PurchaseOrder po = new PurchaseOrder();
        po.setVendorid(newOrder.getVendorid());
        po.setAmount(newOrder.getAmount());
        po.setPodate(new Date());
        entityManager.persist(po);

        for (PurchaseOrderLineitem pol : newOrder.getItems()) {
            PurchaseOrderLineitem npol = new PurchaseOrderLineitem();
            npol.setPoid(po.getId());
            npol.setProductid(pol.getProductid());
            npol.setQty(pol.getQty());
            npol.setPrice(pol.getPrice());
            entityManager.persist(npol);

             Optional<Product> optionalProductp = pr.findById(pol.getProductid());
             if (optionalProductp.isPresent()) {
                 Product p = optionalProductp.get();
                 p.setQoo(p.getQoo()+pol.getQty());
                 entityManager.persist(p);
             }
        }
        return po.getId();
    }

    public PurchaseOrder findPoById(Long id) {
        PurchaseOrder po = entityManager.find(PurchaseOrder.class, id);
        if (po == null) {
            throw new EntityNotFoundException("Can't find PurchaseOrder for ID " + id);
        }
        return po;
    }

    //this method is not used yet
    public PurchaseOrderLineitem findPolById(Long id) {
        PurchaseOrderLineitem pol = entityManager.find(PurchaseOrderLineitem.class, id);
        if (pol == null) {
            throw new EntityNotFoundException("Can't find PurchaseOrderLineItem for ID " + id);
        }
        return pol;

    }
}
