package com.info5059.casestudy.purchaseorder;

import com.info5059.casestudy.product.Product;
import com.info5059.casestudy.product.ProductRepository;
import com.info5059.casestudy.vendor.Vendor;
import com.info5059.casestudy.vendor.VendorRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ReportPDFGenerator - a class for testing how to create dynamic output in PDF
 * format using the iText library
 */
public abstract class PDFGenerator extends AbstractPdfView {


    public static ByteArrayInputStream generateReport(String poid, PurchaseOrderDAO poDAO, VendorRepository vendorRepository, ProductRepository productRepository) {
        URL imageUrl = PDFGenerator.class.getResource("/public/images/vendor1.png");
        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Font catFont = new Font(Font.FontFamily.HELVETICA, 24, Font.BOLD);
        Font smallBold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);

        Font t1bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Font t1nml = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);
        Locale locale = new Locale("en", "US");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);

        try {
            PurchaseOrder po = poDAO.findPoById(Long.parseLong(poid));
            PdfWriter.getInstance(document, baos);
            document.open();
            Paragraph preface = new Paragraph();
// LOGO
            Image image1 = Image.getInstance(imageUrl);
            image1.setAbsolutePosition(55f, 705f);
            preface.add(image1);
            preface.setAlignment(Element.ALIGN_RIGHT);
// Header
            Paragraph mainHead = new Paragraph(String.format("%55s", "PURCHASE ORDER"), catFont);
            preface.add(mainHead);
            preface.add(new Paragraph(String.format("%90s", "PO#:" + poid), t1bold));
            addEmptyLine(preface, 5);
//Vendor
            Optional<Vendor> opt = vendorRepository.findById(po.getVendorid());
            if (opt.isPresent()) {
                Vendor v = opt.get();
                PdfPTable tbl = new PdfPTable(2);
                tbl.setWidthPercentage(40);
                tbl.setHorizontalAlignment(Element.ALIGN_LEFT);
//r1
                PdfPCell c1 = new PdfPCell(new Paragraph("Vendor:", t1bold));
                c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                c1.setBorder(0);
                tbl.addCell(c1);

                c1 = new PdfPCell(new Paragraph(v.getName(), t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c1.setBorder(0);
                tbl.addCell(c1);
//r2
                c1 = new PdfPCell(new Paragraph("", t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(0);
                tbl.addCell(c1);

                c1 = new PdfPCell(new Paragraph(v.getAddress1(), t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c1.setBorder(0);
                tbl.addCell(c1);
//r3
                c1 = new PdfPCell(new Paragraph("", t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(0);
                tbl.addCell(c1);

                c1 = new PdfPCell(new Paragraph(v.getCity(), t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c1.setBorder(0);
                tbl.addCell(c1);
//r4
                c1 = new PdfPCell(new Paragraph("", t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(0);
                tbl.addCell(c1);

                c1 = new PdfPCell(new Paragraph(v.getProvince(), t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c1.setBorder(0);
                tbl.addCell(c1);
//r5
                c1 = new PdfPCell(new Paragraph("", t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBorder(0);
                tbl.addCell(c1);

                c1 = new PdfPCell(new Paragraph(v.getPostalcode(), t1nml));
                c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c1.setBorder(0);
                tbl.addCell(c1);
                preface.add(tbl);
                addEmptyLine(preface, 1);
            }

//Table
            {
                PdfPTable table = new PdfPTable(5);
                //header
                PdfPCell cell = new PdfPCell(new Paragraph("Product Code", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Product Description", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Quantity Sold", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Price", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Ext Price", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                BigDecimal tot = new BigDecimal(0.0);
                BigDecimal taxRate = new BigDecimal(0.13);
                BigDecimal afterTax = new BigDecimal(1.13);

                //rows
                for (PurchaseOrderLineitem line : po.getItems()) {
                    if (line != null) {
                        Optional<Product> optx = productRepository.findById(line.getProductid());
                        if (optx.isPresent()) {
                            Product p = optx.get();
                            //c1
                            cell = new PdfPCell(new Phrase(p.getId()));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            table.addCell(cell);

                            //c2
                            cell = new PdfPCell(new Phrase(p.getName()));
                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                            table.addCell(cell);

                            //c3
                            cell = new PdfPCell(new Phrase("" + line.getQty()));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            table.addCell(cell);

                            //c4
                            cell = new PdfPCell(new Phrase(formatter.format(p.getCostprice())));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            table.addCell(cell);

                            //c5
                            BigDecimal qty = new BigDecimal(line.getQty());
                            BigDecimal price = line.getPrice();
                            price = price.multiply(qty);
                            tot = tot.add(price, new MathContext(8, RoundingMode.UP));
                            cell = new PdfPCell(new Phrase(formatter.format(price)));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            table.addCell(cell);
                        } else {
                            System.out.println(line.getProductid() + "does not exist");
                        }
                    }
                }

                //summary
                {
                    //summary1
                    cell = new PdfPCell(new Phrase("Total:"));
                    cell.setColspan(4);
                    cell.setBorder(0);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(formatter.format(tot)));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);
                    //summary2
                    cell = new PdfPCell(new Phrase("Tax:"));
                    cell.setColspan(4);
                    cell.setBorder(0);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(formatter.format(tot.multiply(taxRate))));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);
                    //summary3
                    cell = new PdfPCell(new Phrase("Order Total:"));
                    cell.setColspan(4);
                    cell.setBorder(0);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(formatter.format(tot.multiply(afterTax))));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(BaseColor.YELLOW);
                    table.addCell(cell);
                }
                preface.add(table);
                addEmptyLine(preface, 3);
            }

//Tail
            preface.setAlignment(Element.ALIGN_CENTER);
            SimpleDateFormat datefmt = new SimpleDateFormat("yyyy-M-dd hh:mm:ss.SSS");
            String strDate = datefmt.format(new Date());
            preface.add(new Paragraph(String.format("%70s", "PO Generated on: " + strDate), t1nml));
            document.add(preface);
            document.close();
        } catch (Exception ex) {
            Logger.getLogger(PDFGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
