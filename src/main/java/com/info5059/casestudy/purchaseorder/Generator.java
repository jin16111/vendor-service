//package com.info5059.casestudy.purchaseorder;
//
//import com.itextpdf.text.*;
//import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
//import org.springframework.web.servlet.view.document.AbstractPdfView;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.net.URL;
//import java.util.Date;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// * Generator - a class for testing how to create dynamic output in PDF
// * format using the iText library
// */
//public abstract class Generator extends AbstractPdfView {
//    public static ByteArrayInputStream generateReport() {
//        URL imageUrl = Generator.class.getResource("/public/images/vendor.jfif");
//        Document document = new Document();
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        Font catFont = new Font(Font.FontFamily.HELVETICA, 24, Font.BOLD);
//        Font subFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
//        Font smallBold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//        try {
//            PdfWriter.getInstance(document, baos);
//            document.open();
//            Paragraph preface = new Paragraph();
//// We add one empty line
//            Image image1 = Image.getInstance(imageUrl);
//            image1.setAbsolutePosition(55f, 760f);
//            preface.add(image1);
//            preface.setAlignment(Element.ALIGN_RIGHT);
//// Lets write a big header
//            Paragraph mainHead = new Paragraph(String.format("%55s", "Some Heading"), catFont);
//            preface.add(mainHead);
//            preface.add(new Paragraph(String.format("%82s", "subhead"), subFont));
//            addEmptyLine(preface, 5);
//// 3 column table
//            PdfPTable table = new PdfPTable(3);
//            PdfPCell cell = new PdfPCell(new Paragraph("table hd 1", smallBold));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(cell);
//            cell = new PdfPCell(new Paragraph("table hd 2", smallBold));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(cell);
//            cell = new PdfPCell(new Paragraph("table hd 3", smallBold));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(cell);
//            cell = new PdfPCell(new Phrase("some"));
//            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            table.addCell(cell);
//            cell = new PdfPCell(new Phrase("important"));
//            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            table.addCell(cell);
//            cell = new PdfPCell(new Phrase("data"));
//            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            table.addCell(cell);
//            cell = new PdfPCell(new Phrase("Total:"));
//            cell.setColspan(2);
//            cell.setBorder(0);
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            table.addCell(cell);
//            cell = new PdfPCell(new Phrase("$9,999.99"));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setBackgroundColor(BaseColor.YELLOW);
//            table.addCell(cell);
//            preface.add(table);
//            addEmptyLine(preface, 3);
//            preface.setAlignment(Element.ALIGN_CENTER);
//            preface.add(new Paragraph(String.format("%60s", new Date()), subFont));
//            document.add(preface);
//            document.close();
//        } catch (Exception ex) {
//            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return new ByteArrayInputStream(baos.toByteArray());
//    }
//    private static void addEmptyLine(Paragraph paragraph, int number) {
//        for (int i = 0; i < number; i++) {
//            paragraph.add(new Paragraph(" "));
//        }
//    }
//}
